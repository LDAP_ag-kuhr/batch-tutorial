# Running programs in parallel

Let's first start with running programs in parallel on a single machine.

We want to generate a bunch of events and write the result back into a root file using the script `scripts/generate_events.py`

```sh
module load root/6.26.00
./scripts/generate_events.py -n 1000 output.root
```

If you look at the output in root you can see it contains a TTree with 1000 entries

```sh
root -l output.root
root [1] .ls
root [2] tree->Print()
```

Now let's emulate that this script (`scripts/generate_events.py`) would take slightly longer by removing the comment on the following line

```python
#time.sleep(10) # simulate a task that takes a little bit
```

If we run it again, it now takes 10 seconds. So that will be 10 seconds for every bunch of 1000 events. Can we parallelize this by running - let's say 4 instances at once?
Of course we can - the simplest solution is to write the command 4 times into a script, lets put the following into `scripts/run_4times.sh`

```sh
./scripts/generate_events.py -n 1000 output1.root & 
./scripts/generate_events.py -n 1000 output2.root &
./scripts/generate_events.py -n 1000 output3.root &
./scripts/generate_events.py -n 1000 output4.root
```

and then run

```sh
chmod +x scripts/run_4times.sh # make script executable
./scripts/run_4times.sh
```

The `&` at the end of the line will cause the command to run in the background, such that we can start another one in parallel. Now, after ~10 seconds we should have 4 output files.
We can add them together with the ROOT commmand `hadd` and see that we now have 4000 events:

```sh
rm output.root
hadd output.root output*.root
root -l output.root
root [1] .ls
root [2] tree->Print()
```

Often we want to restrict the number of tasks running in parallel (e.g. depending on the number of cores we have on our machine). Also we don't want to repeat the line N-times for N parallel tasks. One simple way to do this is the `xargs` command. We combine it with `seq` that spits out a sequence of numbers, e.g.

```
seq 0 9 # numbers from 0 to 9
```

To produce 10 files, running at most 4 in parallel we can now use this command:

```
seq 0 9 | xargs -n 1 -P 4 -I {} -- ./scripts/generate_events.py -n 1000 output{}.root
```

What happened here?
* `seq 0 9` spits out the numbers from 0 to 9, one of them per line
* `|` is a "pipe" that connects two commands and feeds the output of the first one (here `seq`) to the input of the second one (here `xargs`)
* `xargs` builds commands from the input, the arguments here mean the following:
  * `-n 1` use one argument per command (the number for the output file in our case)
  * `-P 4` run at most 4 commands in parallel
  * `-I {}` replace the string `{}` in the following command by the argument
  * `--` indicates we are done with arguments to `xargs` - now follows the actual command
  
For using more complex commands to xargs there is a nice method described in this blog article how one can use shell functions

https://www.oilshell.org/blog/2021/08/xargs.html#xargs-can-invoke-shell-functions-with-the-0-dispatch-pattern

*Notes:*
* for most use cases it makes sense to scale up to roughly the number of physical cores (have a look at the output of `lscpu` - you get the number of physical cores by dividing the number of CPUs by the number of Threads per core.
* When you do this on machines you share with other people be fair!
* Use a terminal multiplexer (such as `tmux`) to be able to log out while your program might still be running

# Using a batch system

If scaling on a single machine is not enough we can use a batch system. At ETP in garching we have a `slurm` batch system in place. Batch systems allow to put jobs into a queue that gets executed in parallel on distributed resources. Users can submit jobs in parallel and there is typically some automatic fair-share balancing. Most of the desktop computers in garching participate as slurm nodes. The jupyterhub server also uses slurm to spawn the instances.

The basic idea is very simple - you use the `sbatch` command to send a job to the queue. You need to specify a partition - use `ls-schaile`:

```sh
sbatch --partition=ls-schaile scripts/generate_events.py -n 1000 output.root
```

check the status with `squeue`:

```sh
squeue
# or just for your username
squeue -u $(whoami)
```

If the cluster is not currently occupied by jobs from other users you should get the resulting output file after the processing time of 10s (+ a bit of overhead). Also there should be a log file called `slurm-<some-number>.out` the messages the command send to the standard output (the print). You can view it with `cat` or `less`.

How did that work?
* By default slurm will capture your current environment variables (that means ROOT and potentially other things you have setup) will work on the Node where the job ends up as well
* The script runs in the directory where you submitted the job from
* Since that directory is on a shared file system everything works out

One has to be a bit careful with this! Oftentimes it is less error prone to deactivate the automatic exporting of environment variables and create a job script that explicitly sets up the environment - and also explicitly copies files to an output directory. If we run the same command again with `--export=NONE` we will get an error message in the slurm log, saying that ROOT was not found.

For an example of that, have a look at `scripts/submit_generate.sh`. We can submit jobs with that in a similar manner:

```
sbatch scripts/submit_generate.sh output.root
```

*Note:* For larger outputs or data you want to share with others you should not use the home directory, but write to one of the project directories. For the ATLAS group that would be `/project/etp` (or etp1,2,3,4,5,6) and for the Belle II group `/project/agkuhr`. Create a directory with your name (possibly shortened) in there before.

Now, to submit many such jobs we could just execute this command multiple times with different output filenames (e.g. in a loop), but the better way are array jobs.
When using `sbatch` you can pass a range of job ids with `-a` that will be available as `$SLURM_ARRAY_TASK_ID` inside the submission script. So, to use this replace this line

```
OUTPUT_FILE=$1
```

by

```sh
OUTPUT_FILE=output_${SLURM_ARRAY_TASK_ID}.root
```

and submit with

```sh
sbatch -a 0-9 scripts/submit_generate.sh
```

This is also useful to retry failed jobs - one can also pass comma separated values to `-a` , e.g. `-a 1,2,3-5` etc. Another useful feature is restricting the number of jobs being queued in parallel, for example

```sh
sbatch -a 0-9%4 scripts/submit_generate.sh
```
will run at most 4 jobs in parallel.

Further information on batch systems and slurm:
* http://www-static.etp.physik.uni-muenchen.de/kurs/Computing/sw/node65.html
* https://wiki.physik.uni-muenchen.de/etp/index.php/Slurm_Batch_on_ETP
* https://www.it.physik.uni-muenchen.de/dienste/rechencluster/index.html
* https://slurm.schedmd.com/slurm.html


## HTCondor on NAF

Another popular batch system is HTCondor - it's available at the NAF at DESY. For instructions see

* https://confluence.desy.de/display/IS/BIRD for general overview
* https://confluence.desy.de/display/IS/Submitting+Jobs for a simple example


## General remarks

* Parallelize only if needed - go for simpler solutions first
* *N times as many processes make N times as many problems* - or more precisely: If the probability of one Node to fail is p then the probability for at least one failure for N nodes is 1 - (1 - p) ^ N.
* For tasks that do frequent reads/writes to files consider copying them to the local scratch space in each job.
