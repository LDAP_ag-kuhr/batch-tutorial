#!/usr/bin/env bash
# we can put parameters for sbatch here, such that we don't need to pass them on the command line each time:
#SBATCH --partition=ls-schaile
#SBATCH --export=NONE # do not export the current environment to the job
#SBATCH --get-user-env # should then be set

# sometimes it's useful to add debug output
echo "Running on $(hostname)"

# load this to make module system available
source /etc/profile.d/modules.sh

# setup
module load root/6.26.00

# in a real application those may be configured to fixed locations on absolute paths
CODE_PATH=$(pwd)
OUTPUT_PATH=$(pwd)

# with the following construct we will create a temporary directory in the local scratch space
# and register a cleanup function that will remove it after the script finishes
WORKDIR=$(mktemp -d /scratch-local/slurm-XXXXXX)
cleanup() {
    echo "cleaning up $WORKDIR"
    cd /tmp
    rm -r $WORKDIR
}
trap cleanup EXIT

cd $WORKDIR
echo "We are running in $WORKDIR"

# if we pass an argument to this script - it will be available as $1
OUTPUT_FILE=$1
${CODE_PATH}/scripts/generate_events.py -n 1000 ${OUTPUT_FILE}

# copy output
echo "Copying ${OUTPUT_FILE} to ${OUTPUT_PATH}"
cp ${OUTPUT_FILE} ${OUTPUT_PATH}