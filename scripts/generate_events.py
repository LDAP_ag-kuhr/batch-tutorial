#!/usr/bin/env python3

import ROOT
import time

def main(output_file, n):
    print("Generating", n, "events and writing to", output_file)
    rdf = ROOT.RDataFrame(n)
    rdf = rdf.Define("x", "gRandom->Gaus()")
    #time.sleep(10) # simulate a task that takes a little bit
    rdf.Snapshot("tree", output_file)

if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser("generate events")
    parser.add_argument("output_file")
    parser.add_argument("-n", help="number of events to generate", type=int, default=1000)
    args = parser.parse_args()

    main(args.output_file, args.n)